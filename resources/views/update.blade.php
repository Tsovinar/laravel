<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
</head>
<body>

<form action="/update" method="put">
    <input type="text">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <button type="submit">SEND</button>

</form>
</body>
</html>